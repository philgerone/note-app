import React from "react";
import { Action, State } from "./state/actions";
import { initialState } from "./state/reducer";

export const NoteContext = React.createContext<{
  state: State;
  dispatch: React.Dispatch<Action>;
}>({
  state: initialState,
  dispatch: () => undefined
});
