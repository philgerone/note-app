import { INote } from "../interfaces/interfaces";

export interface State {
  notes: INote[];
  filterText: string;
  tags: string[];
  verticalLayout: boolean;
}
export enum ActionKind {
  UpdateLayout = "UpdateLayout",
  InitNotes = "InitNotes",
  AddNoteTag = "AddNoteTag",
  RemoveNoteTag = "RemoveNoteTag",
  AddTag = "AddTag",
  RemoveTag = "RemoveTag",
  AddNote = "AddNote",
  UpdateTitle = "UpdateTitle",
  UpdateNote = "UpdateNote",
  RemoveNote = "RemoveNote",
  PinNote = "PinNote",
  SearchNote = "SearchNote"
}

export type Action =
  | { type: ActionKind.UpdateLayout; vertical: boolean }
  | { type: ActionKind.InitNotes; state: State }
  | { type: ActionKind.AddNoteTag; id: number; tag: string }
  | { type: ActionKind.RemoveNoteTag; id: number; tag: string }
  | { type: ActionKind.AddTag; tag: string }
  | { type: ActionKind.RemoveTag; tag: string }
  | { type: ActionKind.AddNote; note: INote }
  | { type: ActionKind.UpdateTitle; id: number; title: string }
  | { type: ActionKind.UpdateNote; id: number; content: string }
  | { type: ActionKind.RemoveNote; id: number }
  | { type: ActionKind.PinNote; id: number }
  | { type: ActionKind.SearchNote; text: string };
