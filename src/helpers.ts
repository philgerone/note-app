import { INote } from "./interfaces/interfaces";

export function createNote(title: string, content: string): INote {
  const creationdate = new Date();

  return {
    id: creationdate.getTime(),
    title,
    creationDate: creationdate,
    content,
    tags: []
  };
}
