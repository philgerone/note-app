import styled from 'styled-components';

interface InputProps {
  backgroundColor?: string;
};
const Input = styled.input<InputProps>`
  margin: 5px;
  border-style: hidden;
  background-color: ${props => props.backgroundColor ?? 'white'};
  :focus {
    outline: none
  }
`;

export default Input