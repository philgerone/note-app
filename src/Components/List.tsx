import React from "react";
import styled from "styled-components";

import { INote } from "../interfaces/interfaces";
import Note from "./Note/Note";

interface WrapperProps {
  vertical: boolean;
}

const Wrapper = styled.div<WrapperProps>`
  display: flex;
  flex-direction: ${(props) => {
    const direction = props.vertical ? "column" : "row";
    return direction;
  }};
`;
const WrapperNote = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: flex-start;
  & > * {
    margin: 5px;
  }
`;

interface Props {
  notes: INote[];
  tags: string[];
  vertical?: boolean;
  onUpdateTitle: (id: number, title: string) => void;
  onUpdateNote: (id: number, content: string) => void;
  onDeleteNote: (id: number) => void;
  onPinNote: (id: number) => void;
  onAddTag: (id: number, tag: string) => void;
  onDeleteTag: (id: number, tag: string) => void;
}

const List = ({
  notes,
  tags,
  onUpdateTitle,
  onUpdateNote,
  onDeleteNote,
  onPinNote,
  onAddTag,
  onDeleteTag,
  vertical = false
}: Props) => {
  const pinned = notes.filter((note) => note.pinned);
  const arePinned = pinned.length !== 0;
  const notPinned = notes.filter((note) => !note.pinned);

  const notesList = (notes: INote[]) => {
    return (
      <>
        {notes.map((note) => {
          return (
            <Note
              key={note.id}
              note={note}
              tags={tags}
              onContentChange={(content) => onUpdateNote(note.id, content)}
              onTitleChange={(title) => onUpdateTitle(note.id, title)}
              onDeleteNote={() => onDeleteNote(note.id)}
              onPinNote={() => onPinNote(note.id)}
              onAddTag={(tag) => onAddTag(note.id, tag)}
              onDeleteTag={(tag) => onDeleteTag(note.id, tag)}
            />
          );
        })}
      </>
    );
  };

  return (
    <Wrapper vertical={vertical}>
      {arePinned && (
        <>
          <div>PINNED</div>
          <WrapperNote>{notesList(pinned)}</WrapperNote>
          <div>OTHERS</div>
        </>
      )}

      <WrapperNote>{notesList(notPinned)}</WrapperNote>
    </Wrapper>
  );
};

export default List;
