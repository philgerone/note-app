import { Action, ActionKind, State } from "./actions";

function saveState(state: State) {
  try {
    localStorage.setItem("myNotes", JSON.stringify(state));
  } catch (error) {}
}

const reducer = (state: State, action: Action): State => {
  const { notes, tags } = state;
  switch (action.type) {
    case ActionKind.UpdateLayout: {
      const newState = { ...state, verticalLayout: action.vertical };
      saveState(newState);
      return newState;
    }

    case ActionKind.InitNotes: {
      return { ...action.state };
    }

    case ActionKind.AddNoteTag: {
      const note = notes.find((note) => note.id === action.id);
      if (note) {
        note.tags = note.tags?.concat(action.tag);

        const newState = { ...state, notes: notes.slice() };
        saveState(newState);
        return newState;
      }
      return state;
    }

    case ActionKind.RemoveNoteTag: {
      const note = notes.find((note) => note.id === action.id);
      if (note) {
        note.tags = note.tags?.filter((tag) => tag !== action.tag);

        const newState = { ...state, notes: notes.slice() };
        saveState(newState);
        return newState;
      }
      return state;
    }

    case ActionKind.AddTag: {
      const newTags = [action.tag].concat(tags);
      const newState = { ...state, tags: newTags };
      saveState(newState);
      return newState;
    }

    case ActionKind.SearchNote: {
      return { ...state, filterText: action.text };
    }

    case ActionKind.PinNote: {
      const note = notes.find((note) => note.id === action.id);
      note && (note.pinned = !note.pinned);

      const newState = { ...state, notes: notes.slice() };
      saveState(newState);
      return newState;
    }

    case ActionKind.AddNote: {
      const newNotes = [action.note].concat(notes);
      const newState = { ...state, notes: newNotes };
      saveState(newState);
      return newState;
    }

    case ActionKind.UpdateTitle: {
      const note = notes.find((note) => note.id === action.id);
      if (note) {
        note.title = action.title;

        const newState = { ...state, notes: notes.slice() };
        saveState(newState);
        return newState;
      }
      return state;
    }

    case ActionKind.UpdateNote: {
      const note = notes.find((note) => note.id === action.id);
      if (note) {
        note.content = action.content;

        const newState = { ...state, notes: notes.slice() };
        saveState(newState);
        return newState;
      }
      return state;
    }

    case ActionKind.RemoveNote: {
      const newNotes = notes.filter((note) => note.id !== action.id);
      const newState = { ...state, notes: newNotes };
      saveState(newState);
      return newState;
    }

    default:
      return state;
  }
};

const initialState: State = {
  notes: [],
  filterText: "",
  tags: ["todo", "shopping"],
  verticalLayout: false
};

export { ActionKind, initialState };
export default reducer;
