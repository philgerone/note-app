import { useEditor, EditorContent } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import { useState } from "react";
import MenuBar from "./MenuBar";

import "./TipTap.css";

interface Props {
  content: string;
  onUpdate: (content: string) => void;
  onFocus: () => void;
  onBlur: () => void;
}

const Tiptap = ({ content, onUpdate, onFocus, onBlur }: Props) => {
  const [edited, setEdited] = useState<boolean>(false);

  const editor = useEditor({
    onUpdate({ editor }) {
      const newContent = editor.getHTML();

      onUpdate(newContent);
    },
    onFocus({ editor, event }) {
      setEdited(true);
      onFocus();
    },
    onBlur({ editor, event }) {
      setEdited(false);
      onBlur();
    },
    extensions: [StarterKit],
    content
  });

  return (
    <>
      <MenuBar editor={editor} />
      <EditorContent editor={editor} />
    </>
  );
};

export default Tiptap;
