export interface INote {
  id: number;
  title: string;
  creationDate: Date;
  updateDate?: Date;
  content: string;
  pinned?: boolean;
  tags?: string[];
}
