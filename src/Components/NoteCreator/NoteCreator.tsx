import React from "react";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

import { INote } from "../../interfaces/interfaces";
import useNoteCreator from "./useNoteCreator";

import Input from "../UiLib/Input";
interface Props {
  onNoteAdded: (note: INote) => void;
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: space-between;
  padding: 1em;
  border-radius: 15px;
  color: #cccccc;
  border: 1px solid #cccccc;
  width: 300px;
  box-shadow: 0 2px 7px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);
`;
const BottomWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const NoteCreator = ({ onNoteAdded }: Props) => {
  const {
    node,
    inputTitle,
    inputValue,
    editing,
    actions: {
      handleCloseClick,
      handleClick,
      handleKeyUp,
      handleTitleChange,
      handleNoteChange
    }
  } = useNoteCreator({ onNoteAdded });

  return (
    <Wrapper ref={node}>
      {editing && (
        <Input
          type="text"
          placeholder={"Title"}
          value={inputTitle}
          onChange={handleTitleChange}
        />
      )}
      <Input
        type="text"
        placeholder={"Take a note..."}
        value={inputValue}
        onKeyUp={handleKeyUp}
        onClick={handleClick}
        onChange={handleNoteChange}
      />
      {editing && (
        <BottomWrapper>
          <Button onClick={handleCloseClick}>Close</Button>
        </BottomWrapper>
      )}
    </Wrapper>
  );
};

export default NoteCreator;
