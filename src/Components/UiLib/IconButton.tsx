import styled from "styled-components";

interface Props {
  active?: boolean;
}
const IconButton = styled.button<Props>`
  padding: 0.5em;
  border-radius: 15px;
  border: none;
  background: ${(props) => (props.active ? "lightGrey" : "none")};
  :hover {
    background: lightgrey;
  }
`;

export default IconButton;
