import React, { ReactNode } from "react";
import styled from "styled-components";

interface Props {
  toolbar?: ReactNode;
  header: ReactNode;
  content: ReactNode;
}

const Wrapper = styled.div`
  margin: 5px;
  display: flex;
  flex-direction: column;
`;

const Tag = ({ toolbar, header, content }: Props) => {
  return <Wrapper></Wrapper>;
};

export default Tag;
