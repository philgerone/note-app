import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders the note app", () => {
  render(<App />);

  expect(screen.getByPlaceholderText(/search/i)).toBeInTheDocument();
  expect(screen.getByPlaceholderText(/take a note.../i)).toBeInTheDocument();
});
