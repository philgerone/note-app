import React, { useState } from "react";
import styled from "styled-components";
import { useEditor, EditorContent } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import { Chip, Menu, MenuItem, Tooltip } from "@material-ui/core";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faThumbtack, faTag } from "@fortawesome/free-solid-svg-icons";

import IconButton from "../UiLib/IconButton";
import Input from "../UiLib/Input";
import { INote } from "../../interfaces/interfaces";
import MenuBar from "../MenuBar";

interface WrapperProps {
  hideFocusState?: boolean;
}
const Wrapper = styled.div<WrapperProps>`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1em;
  width: 200px;
  border-radius: 15px;
  border: 1px solid #cccccc;
  position: relative;
  :hover {
    box-shadow: ${(props) =>
      props.hideFocusState
        ? ""
        : "0 1px 6px 0 rgba(0, 0, 0, 0.2), 0 2px 9px 0 rgba(0, 0, 0, 0.19)"};
  }
`;

interface WrapperPinProps {
  hide?: boolean;
}
const WrapperPin = styled.div<WrapperPinProps>`
  position: absolute;
  top: 10px;
  right: 10px;
  visibility: ${(props) => (props.hide ? "hidden" : "visible")};
`;

interface WrapperHideProps {
  hide?: boolean;
}
const WrapperHide = styled.div<WrapperHideProps>`
  visibility: ${(props) => (props.hide ? "hidden" : "visible")};
`;

interface Props {
  note: INote;
  tags: string[];
  hideFocusState?: boolean;
  onTitleChange: (title: string) => void;
  onContentChange: (content: string) => void;
  onDeleteNote: () => void;
  onPinNote: () => void;
  onAddTag: (tag: string) => void;
  onDeleteTag: (tag: string) => void;
}

const Note = ({
  note: { title, content, pinned, tags: noteTags },
  tags,
  hideFocusState,
  onTitleChange,
  onContentChange,
  onDeleteNote,
  onPinNote,
  onAddTag,
  onDeleteTag
}: Props) => {
  const [inputTitle, setInputTitle] = useState<string>(title);
  const [focused, setFocused] = useState<boolean>(false);
  const [edited, setEdited] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleMenuClick = (tag: string) => {
    onAddTag(tag);
    setAnchorEl(null);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const title = e.target.value;
    setInputTitle(title);
    onTitleChange(title);
  };
  const handleMouseEnter = () => {
    setFocused(true);
  };
  const handleMouseLeave = () => {
    setFocused(false);
  };

  const editor = useEditor({
    onUpdate({ editor }) {
      const newContent = editor.getHTML();

      onContentChange(newContent);
    },
    onFocus({ editor, event }) {
      setEdited(true);
    },
    onBlur({ editor, event }) {
      setEdited(false);
    },
    extensions: [StarterKit],
    content
  });

  const handleDeleteTag = (tag: string) => {
    onDeleteTag(tag);
  };

  const handleShowTags = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const _tags = tags?.filter((tag) => !noteTags?.includes(tag));

  const showPinned = pinned || focused || edited;
  const showIcons = focused || edited;
  return (
    <Wrapper
      hideFocusState={hideFocusState}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}>
      <WrapperPin hide={!showPinned}>
        <Tooltip title={pinned ? "Unpin" : "Pin"}>
          <IconButton onClick={onPinNote}>
            <FontAwesomeIcon icon={faThumbtack} />
          </IconButton>
        </Tooltip>
      </WrapperPin>
      <Input
        type="text"
        placeholder={"Title"}
        value={inputTitle}
        onChange={handleTitleChange}
      />
      <WrapperHide hide={!focused}>
        <MenuBar editor={editor} />
      </WrapperHide>

      <EditorContent editor={editor} />

      <WrapperHide hide={!showIcons}>
        <Tooltip title="Delete">
          <IconButton onClick={onDeleteNote}>
            <FontAwesomeIcon icon={faTrash} />
          </IconButton>
        </Tooltip>
        <Tooltip title="Tags">
          <IconButton onClick={handleShowTags}>
            <FontAwesomeIcon icon={faTag} />
          </IconButton>
        </Tooltip>
      </WrapperHide>

      <div>
        {noteTags &&
          noteTags.map((tag) => {
            return (
              <Chip
                key={tag}
                size="small"
                label={tag}
                onDelete={() => handleDeleteTag(tag)}
              />
            );
          })}
      </div>

      <Menu
        id="tags-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}>
        {_tags.map((tag) => {
          return (
            <MenuItem key={tag} onClick={() => handleMenuClick(tag)}>
              {tag}
            </MenuItem>
          );
        })}
      </Menu>
    </Wrapper>
  );
};

export default Note;
