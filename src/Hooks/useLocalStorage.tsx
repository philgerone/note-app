import { Dispatch, useEffect } from "react";
import { Action, ActionKind } from "../state/actions";

// local storage read/write
const useLocalStorage = (dispatch: Dispatch<Action>) => {
  useEffect(() => {
    const raw: string | null = localStorage.getItem("myNotes");
    if (raw) {
      try {
        const state = JSON.parse(raw);
        dispatch({ type: ActionKind.InitNotes, state });
      } catch (ex) {
        console.log("🚀 ~ file: useNotes.ts ~ line 14 ~ useEffect ~ ex", ex);
      }
    }
  }, [dispatch]);
};

export default useLocalStorage;
