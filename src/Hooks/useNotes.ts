import { useReducer } from "react";

import { INote } from "../interfaces/interfaces";
import reducer, { ActionKind, initialState } from "../state/reducer";

function useNotes() {
  const [state, dispatch] = useReducer(reducer, initialState);

  const addNoteTag = (id: number, tag: string) => {
    dispatch({ type: ActionKind.AddNoteTag, id, tag });
  };

  const removeNoteTag = (id: number, tag: string) => {
    dispatch({ type: ActionKind.RemoveNoteTag, id, tag });
  };

  const addTag = (tag: string) => {
    dispatch({ type: ActionKind.AddTag, tag });
  };

  const addNote = (note: INote) => {
    dispatch({ type: ActionKind.AddNote, note });
  };

  const updateTitle = (id: number, title: string) => {
    dispatch({ type: ActionKind.UpdateTitle, id, title });
  };

  const updateNote = (id: number, content: string) => {
    dispatch({ type: ActionKind.UpdateNote, id, content });
  };

  const removeNote = (id: number) => {
    dispatch({ type: ActionKind.RemoveNote, id });
  };

  const pinNote = (id: number) => {
    dispatch({ type: ActionKind.PinNote, id });
  };

  const searchNote = (text: string) => {
    dispatch({ type: ActionKind.SearchNote, text });
  };

  return {
    state,
    dispatch,
    actions: {
      addNoteTag,
      removeNoteTag,
      addTag,
      addNote,
      updateTitle,
      updateNote,
      removeNote,
      pinNote,
      searchNote
    }
  };
}

export default useNotes;
