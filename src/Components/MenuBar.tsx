import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBold, faItalic, faList } from "@fortawesome/free-solid-svg-icons";

import { Editor } from "@tiptap/react";
import styled from "styled-components";
import IconButton from "./UiLib/IconButton";

const Wrapper = styled.div`
  margin: 5px;
  display: flex;
  flex-direction: row;
`;

interface Props {
  editor: Editor | null;
}

const MenuBar = ({ editor }: Props) => {
  if (!editor) {
    return null;
  }

  return (
    <Wrapper>
      <IconButton
        active={editor.isActive("bold")}
        onClick={() => editor.chain().focus().toggleBold().run()}>
        <FontAwesomeIcon icon={faBold} />
      </IconButton>
      <IconButton
        active={editor.isActive("italic")}
        onClick={() => editor.chain().focus().toggleItalic().run()}>
        <FontAwesomeIcon icon={faItalic} />
      </IconButton>
      <IconButton
        active={editor.isActive("bulletList")}
        onClick={() => editor.chain().focus().toggleBulletList().run()}>
        <FontAwesomeIcon icon={faList} />
      </IconButton>
    </Wrapper>
  );
};

export default MenuBar;
