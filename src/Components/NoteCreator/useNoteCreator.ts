import { useEffect, useRef, useState } from "react";
import { createNote } from "../../helpers";
import { INote } from "../../interfaces/interfaces";

interface INodeCreator {
  onNoteAdded: (note: INote) => void;
}
const useNoteCreator = ({ onNoteAdded }: INodeCreator) => {
  const node = useRef<HTMLDivElement>(null);
  const [editing, setEditing] = useState<boolean>(false);
  const [inputTitle, setInputTitle] = useState<string>("");
  const [inputValue, setInputValue] = useState<string>("");

  // detects if the user clicked outside of the NoteCreator
  useEffect(() => {
    const handleDivClick = (e: MouseEvent) => {
      if (node.current && node.current.contains(e.target as HTMLDivElement)) {
        // inside click
        return;
      }
      // outside click
      setEditing(false);

      const isNewNoteCreated =
        inputTitle.length !== 0 || inputValue.length !== 0;
      if (isNewNoteCreated) {
        const note = createNote(inputTitle, inputValue);
        resetValues();
        onNoteAdded(note);
      }

      resetValues();
    };

    document.addEventListener("mousedown", handleDivClick);

    return () => {
      document.removeEventListener("mousedown", handleDivClick);
    };
  }, [onNoteAdded, inputTitle, inputValue]);

  const resetValues = () => {
    setInputTitle("");
    setInputValue("");
  };

  const handleNoteAdded = () => {
    setEditing(false);

    if (!inputTitle && !inputValue) return;

    const note = createNote(inputTitle, inputValue);
    resetValues();
    onNoteAdded(note);
  };

  const handleCloseClick = () => {
    handleNoteAdded();
  };
  const handleClick = () => {
    setEditing(true);
  };
  const handleKeyUp = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    if (e.key === "Enter") {
      handleNoteAdded();
    }
  };
  const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputTitle(e.target.value);
  };
  const handleNoteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  return {
    node,
    inputTitle,
    inputValue,
    editing,
    actions: {
      handleCloseClick,
      handleClick,
      handleKeyUp,
      handleTitleChange,
      handleNoteChange
    }
  };
};

export default useNoteCreator;
