import React, { useState } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flew;
  align-items: center;
  border-radius: 15px;
  color: #cccccc;
  border: 1px solid #cccccc;
  width: 300px;
`;
const Input = styled.input`
  margin: 5px;
  border-style: hidden;
  width: calc(100% - 15px);
  :focus {
    outline: none;
  }
`;

interface Props {
  onSearchNote: (text: string) => void;
}

const SearchBar = ({ onSearchNote }: Props) => {
  const [inputValue, setInputValue] = useState<string>("");

  const handleNoteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const text: string = e.target.value;
    setInputValue(text);
    onSearchNote(text);
  };

  return (
    <Wrapper>
      <Input
        type="text"
        placeholder={"Search"}
        value={inputValue}
        onChange={handleNoteChange}
      />
    </Wrapper>
  );
};

export default SearchBar;
