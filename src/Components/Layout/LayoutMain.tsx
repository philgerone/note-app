import { ReactNode } from "react";
import styled from "styled-components";

interface Props {
  toolbar?: ReactNode;
  header: ReactNode;
  content: ReactNode;
}

const Wrapper = styled.div`
  margin: 5px;
  display: flex;
  flex-direction: column;
  > * {
    margin-bottom: 5px;
  }
`;
const WrapperHeader = styled.div`
  display: flex;
  justify-content: center;
`;

const Layoutmain = ({ toolbar, header, content }: Props) => {
  return (
    <Wrapper>
      {toolbar}
      <WrapperHeader>{header}</WrapperHeader>
      {content}
    </Wrapper>
  );
};

export default Layoutmain;
