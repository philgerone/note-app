import { useMemo } from "react";

import "./App.css";

import { LayoutMain, List, NoteCreator, Toolbar } from "./Components";
import { NoteContext } from "./context";
import { ActionKind } from "./state/actions";
import useLocalStorage from "./Hooks/useLocalStorage";
import useNotes from "./Hooks/useNotes";

function App() {
  const {
    state,
    dispatch,
    actions: {
      addNote,
      updateTitle,
      updateNote,
      removeNote,
      pinNote,
      addNoteTag,
      removeNoteTag,
      searchNote
    }
  } = useNotes();

  useLocalStorage(dispatch);

  const { notes, filterText } = state;

  const _notes = useMemo(() => {
    return filterText.length === 0
      ? notes
      : notes.filter(
          (note) =>
            note.tags?.includes(filterText) ||
            note.title === filterText ||
            note.content.includes(filterText)
        );
  }, [filterText, notes]);

  const handleHorizontal = () => {
    dispatch({ type: ActionKind.UpdateLayout, vertical: false });
  };
  const handleVertical = () => {
    dispatch({ type: ActionKind.UpdateLayout, vertical: true });
  };
  return (
    <NoteContext.Provider value={{ state, dispatch }}>
      <LayoutMain
        toolbar={
          <Toolbar
            onSearchNote={searchNote}
            vertical={state.verticalLayout}
            onHorizontal={handleHorizontal}
            onVertical={handleVertical}
          />
        }
        header={<NoteCreator onNoteAdded={addNote} />}
        content={
          <List
            notes={_notes}
            vertical={state.verticalLayout}
            tags={state.tags}
            onUpdateTitle={updateTitle}
            onUpdateNote={updateNote}
            onDeleteNote={removeNote}
            onPinNote={pinNote}
            onAddTag={addNoteTag}
            onDeleteTag={removeNoteTag}
          />
        }
      />
    </NoteContext.Provider>
  );
}

export default App;
