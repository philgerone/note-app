import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import NoteCreator from "./NoteCreator";

test("renders the note app", () => {
  const onNoteAddedSpy = jest.fn();
  render(<NoteCreator onNoteAdded={onNoteAddedSpy} />);

  userEvent.type(screen.getByPlaceholderText(/take a note.../i), "my note");
  userEvent.click(screen.getByRole("button", { name: /close/i }));

  expect(onNoteAddedSpy).toHaveBeenCalled();
});
