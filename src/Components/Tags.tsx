import { useContext, useState } from "react";
import styled from "styled-components";
import CreatableSelect from "react-select/creatable";
import { NoteContext } from "../context";
import { ActionKind } from "../state/reducer";

interface CreatableEditableSelectOption {
  label: string;
  value: string;
}
type CreatableEditableSelectValue = CreatableEditableSelectOption;

const Wrapper = styled.div`
  width: 200px;
`;

const createOption = (label: string): CreatableEditableSelectOption => ({
  label,
  value: label.toLowerCase().replace(/\W/g, "")
});

const Tags = () => {
  const {
    state: { tags },
    dispatch
  } = useContext(NoteContext);
  const [value, setValue] = useState<CreatableEditableSelectValue[]>([]);

  const handleChange = (newValue: any, actionMeta: any) => {
    setValue(newValue);

    if (actionMeta.action === "create-option") {
      const exist = tags.find((tag) => tag === newValue.label);
      if (!exist) {
        dispatch({ type: ActionKind.AddTag, tag: newValue.label });
      }
    }
  };

  const options = tags.map(createOption);
  return (
    <Wrapper>
      <CreatableSelect
        isMulti={false}
        placeholder={"Tags"}
        onChange={handleChange}
        options={options}
        value={value}
      />
    </Wrapper>
  );
};

export default Tags;
