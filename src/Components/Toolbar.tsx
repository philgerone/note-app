import React from "react";
import styled from "styled-components";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGripHorizontal,
  faGripVertical
} from "@fortawesome/free-solid-svg-icons";
import { Tooltip } from "@material-ui/core";
import SearchBar from "./SearchBar";
import Tags from "./Tags";
import IconButton from "./UiLib/IconButton";

const WrapperRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 1em;
  padding: 1em;
  border-bottom: 1px solid #cccccc;
`;
const WrapperToolbar = styled.div`
  display: flex;
  > * {
    margin-left: 5px;
  }
  > *:first-child {
    margin-left: 0px;
  }
`;
interface Props {
  vertical?: boolean;
  onSearchNote: (text: string) => void;
  onHorizontal: () => void;
  onVertical: () => void;
}

const Toolbar = ({
  vertical = false,
  onSearchNote,
  onHorizontal,
  onVertical
}: Props) => {
  return (
    <WrapperRow>
      <WrapperToolbar>
        <SearchBar onSearchNote={onSearchNote} />
        <Tags />
      </WrapperToolbar>
      <div>
        {vertical && (
          <Tooltip title="Grid view">
            <IconButton onClick={onHorizontal}>
              <FontAwesomeIcon icon={faGripHorizontal} />
            </IconButton>
          </Tooltip>
        )}
        {!vertical && (
          <Tooltip title="List view">
            <IconButton onClick={onVertical}>
              <FontAwesomeIcon icon={faGripVertical} />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </WrapperRow>
  );
};

export default Toolbar;
